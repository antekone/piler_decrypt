#!/usr/bin/env bash

if [[ "`id -u`" != "0" ]]; then
    echo "Use root, luke!"
    exit 1
fi

echo "Starting, ssh 3322"
podman run -v `pwd`/..:/src -p 3322:22 jerry_dev

container_id="`podman ps -a -f ancestor='localhost/jerry_dev' -f status='exited' -l --noheading | cut -d' ' -f1`"

echo Last container: $container_id

echo "Save settings:"
echo "sudo podman commit $container_id localhost/jerry_dev"

