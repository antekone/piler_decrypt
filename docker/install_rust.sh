#!/usr/bin/env bash

wget https://sh.rustup.rs -O installer
chmod +x installer
./installer -y

mkdir ~/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEArn2mitHsvcTlEggDwoR+aF106tT76rJbofGWxqfBeL7h/Z556c0wGKyM/oDJhmR6iQvIh9b+IIjeeMy17XQiT6qz2zsvzRTRdalQpO/eM7+/QBaqN+ZBjT5o8+kKnMLwS4y0KTvMBDLo8IsyyErJ+bELahP4tpq31hjlW2HixvEnFzl7JWgqY7eVUC9krVjGVRfHumZTuFFhGwVSZX2QNnT1EWeZtF9BO+MO3HOqt+2wIHuCw8PQROYdEwFBbWXYRqWDtGjHkySlpx6tGiDVHqW4+X9Kvbssl/X7ogtYoxHE+z3RQzQAj1zJZ9pMh/KHOLy9clKoSCJkx+sT/68CSQ== antek@hydra" > ~/.ssh/authorized_keys
