#!/usr/bin/env bash

set -euo pipefail
set -x

container_id="`sudo podman ps -a -f ancestor='localhost/jerry_dev' -f status='running' -l --noheading | cut -d' ' -f1`"
if [[ "$container_id" != "" ]]; then
    sudo podman stop $container_id
else
    echo "Can't stop!"
fi
