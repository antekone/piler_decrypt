#![allow(dead_code, unused)]

use std::{fs, thread};
use std::collections::HashMap;
use std::fs::OpenOptions;
use std::io::{Read, Write};
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use std::sync::mpsc::sync_channel;
use std::thread::sleep;
use std::time::Duration;

use clap::{Args, Parser, Subcommand};
use flate2::read::ZlibDecoder;
use log::*;
use memmem::{Searcher, TwoWaySearcher};
use mmap_rs::MmapOptions;
use openssl::symm::{Cipher, decrypt};
use threadpool::ThreadPool;
use walkdir::WalkDir;

use crate::Command::{Decrypt, Sort};
use crate::decrypt::DecryptArgs;
use crate::sort::SortArgs;

mod decrypt;
mod sort;
mod utils;

#[derive(Parser)]
#[command(about = "Decrypts Piler repository, stitches attachments \
    into EML files, and generates a Maildir-like e-mail repository")]
struct Cli {
    #[command(subcommand)]
    command: Option<Command>,

    /// Be noisy during work (for debug purposes)
    #[arg(long, short)]
    verbose: bool,
}

#[derive(Subcommand)]
enum Command {
    Decrypt(DecryptArgs),
    Sort(SortArgs)
}

#[derive(thiserror::Error, Debug)]
pub enum AppError {
    #[error("{0}")]
    Generic(String),
    #[error("I/O: {0}")]
    IO(#[from] std::io::Error),
    #[error("Directory walker: {0}")]
    WalkDir(#[from] walkdir::Error),
    #[error("Memory mapping error: {0}")]
    Mmap(#[from] mmap_rs::Error),
    #[error("Decryption error: {0}")]
    OpenSSL(#[from] openssl::error::ErrorStack),
    #[error("Serialization/deserialization error: {0}")]
    Serde(#[from] serde_json::Error)
}

pub type AppResult<OK> = Result<OK, AppError>;

impl AppError {
    fn generic<OK>(s: impl AsRef<str>) -> AppResult<OK> {
        Err(AppError::Generic(s.as_ref().to_string()))
    }
}


fn setup_log(verbose: bool) {
    simple_logger::SimpleLogger::new().init().unwrap();
    if verbose {
        log::set_max_level(LevelFilter::Trace);
    } else {
        log::set_max_level(LevelFilter::Info);
    }
}

fn main() {
    do_main();
}

fn do_main() {
    let args = Cli::parse();
    setup_log(args.verbose);

    let r = match args.command {
        Some(Decrypt(args)) => decrypt::run(&args),
        Some(Sort(args)) => sort::run(&args),
        None => {
            error!("Missing command, please use `--help` to get some help.");
            std::process::exit(1);
        }
    };

    let return_value: i32 = match r {
        Ok(_) => 0,
        Err(msg) => {
            error!("Error: {}", msg);
            1
        }
    };

    std::process::exit(return_value);
}