use std::fs;
use std::fs::OpenOptions;
use std::io::{Read, Write};
use std::path::PathBuf;
use std::sync::Arc;
use std::thread::sleep;
use std::time::Duration;

use flate2::bufread::ZlibDecoder;
use log::{error, info, trace, warn};
use memmem::{Searcher, TwoWaySearcher};
use mmap_rs::MmapOptions;
use openssl::symm::Cipher;
use openssl::symm::decrypt;
use threadpool::ThreadPool;
use walkdir::WalkDir;

use crate::{AppError, AppResult, Args};

/// Decrypt Piler e-mails into one huge directory with EML files
#[derive(Args)]
pub struct DecryptArgs {
    /// A path to Piler's e-mail repository (with .m files)
    #[arg(short, long, value_name = "DIR PATH")]
    store_path: String,

    /// Path to the `piler.key` file
    #[arg(short, long, value_name = "FILE PATH")]
    piler_key_path: String,

    /// Output directory for decrypted e-mails
    #[arg(short, long, value_name = "DIR PATH")]
    output_path: String,

    /// Use this number of cores (default = detect automatically)
    #[arg(short, long, value_name = "NUMBER OF CORES")]
    num_cores: Option<usize>,

    /// Limit number of recovered e-mails
    #[arg(short, long, value_name = "COUNT")]
    limit: Option<usize>,
}

fn extract_attachment_info(data: &[u8]) -> Option<(usize, String)> {
    let data_id = &data[19..];
    let mut filename_size = 0;
    for i in 0..99 {
        if data_id[i] == '.' as u8 {
            break;
        } else {
            filename_size += 1;
        }
    }

    if data_id[filename_size] == 'a' as u8 {
        error!("Attachment extraction failed: can't extract attachment filename");
        return None;
    }

    let data_att_count = &data[(19 + filename_size + 2)..];
    let mut att_count_size = 0;
    for i in 0..99 {
        if data_att_count[i] >= '0' as u8 && data_att_count[i] <= '9' as u8 {
            att_count_size += 1;
        } else {
            break;
        }
    }

    if att_count_size == 0 || att_count_size > 10 {
        error!("Attachment extraction failed: can't extract attachment number");
        return None;
    }

    let data_filename = &data[19..19 + filename_size];
    let data_att_number = &data[(19 + filename_size + 2)..(19 + filename_size + 2 + att_count_size)];

    let filename = String::from_utf8_lossy(data_filename).to_string();
    let att_number = String::from_utf8_lossy(data_att_number).to_string();

    Some((19 + filename_size + 2 + att_count_size + 10, format!("{}.a{}", filename, att_number)))
}

fn resolve_attachments(m_path: &PathBuf, output_dir: &String, data: &[u8], key: &[u8]) -> AppResult<(Vec<u8>, bool)> {
    let search = TwoWaySearcher::new("ATTACHMENT_POINTER_".as_bytes());
    let mut beg = 0;
    let mut end = data.len();
    let mut att_seq = Vec::<(usize, usize, Vec<u8>)>::new();
    let mut errors = false;

    loop {
        let pos = search.search_in(&data[beg..end]);
        if pos.is_none() { break; }
        let abs_pos = pos.unwrap() + beg;
        beg = abs_pos + 1;

        if let Some((idx_end, filename)) = extract_attachment_info(&data[abs_pos..end]) {
            let mut m_path_root = m_path.clone();
            let filename_path: PathBuf = (&filename).into();
            let file_stem = filename_path.file_stem().unwrap().to_str().unwrap();

            m_path_root.pop();
            m_path_root.pop();
            m_path_root.pop();
            m_path_root.pop();

            let filename_len = file_stem.len();
            let token0 = &file_stem[filename_len-28..filename_len-25];
            let token1 = &file_stem[filename_len-4..filename_len-2];
            let token2 = &file_stem[filename_len-2..filename_len];

            m_path_root.push(token0);
            m_path_root.push(token1);
            m_path_root.push(token2);
            m_path_root.push(filename);

            let att_data = match decrypt_attachment(&m_path_root, key, output_dir) {
                Ok(decrypted) => decrypted,
                Err(msg) => {
                    trace!("Unknown attachment: {:?}", m_path_root);
                    errors = true;
                    vec![0x0D, 0x0A]
                }
            };

            att_seq.push((abs_pos, idx_end, att_data));
        }
    }

    if att_seq.is_empty() { return Ok((data.to_vec(), false)); }

    let mut rendered_buf = Vec::new();

    beg = 0;
    for (index, sig_size, att_data) in att_seq.iter() {
        end = *index;
        rendered_buf.write_all(&data[beg..end]);
        rendered_buf.write_all(att_data);
        beg = *index + sig_size;
    }

    rendered_buf.write_all(&data[beg..]);
    Ok((rendered_buf, errors))
}

fn decrypt_attachment(path: &PathBuf, key: &[u8], output_dir: &String) -> AppResult<Vec<u8>> {
    let path_str = path.to_str().unwrap();
    let path_str = path_str.to_string();

    let mut output_path = PathBuf::new();
    output_path.push(&output_dir);
    output_path.push(path.file_name().unwrap().to_str().unwrap());

    if output_path.exists() {
        let file = OpenOptions::new().read(true).write(false).open(&output_path)?;
        let data = unsafe {
            MmapOptions::new(file.metadata()?.len() as usize)
                .with_file(file, 0u64)
                .map()?
        };

        Ok(data.to_vec())
    } else {
        let file = OpenOptions::new().read(true).write(false).open(&path)?;
        let data = unsafe {
            MmapOptions::new(file.metadata()?.len() as usize)
                .with_file(file, 0u64)
                .map()?
        };

        let cipher = Cipher::bf_cbc();
        let decrypted = decrypt(cipher, &key[..cipher.key_len()], None, data.as_slice())?;
        let mut zlib = ZlibDecoder::new(decrypted.as_slice());
        let mut uncompressed = Vec::new();
        zlib.read_to_end(&mut uncompressed)?;
        fs::File::create(output_path)?.write_all(&uncompressed)?;
        Ok(uncompressed)
    }
}

fn decrypt_file(path: PathBuf, key: Arc<Vec<u8>>, output_dir: String) -> AppResult<usize> {
    let path_str = path.to_str().unwrap();
    let path_str = path_str.to_string();

    let mut output_path = PathBuf::new();
    output_path.push(&output_dir);
    output_path.push(format!("{}.eml", path.file_stem().unwrap().to_str().unwrap()));
    if output_path.exists() {
        return Ok(0)
    }

    let file = OpenOptions::new().read(true).write(false).open(&path)?;
    let data = unsafe {
        MmapOptions::new(file.metadata()?.len() as usize)
            .with_file(file, 0u64)
            .map()?
    };

    let cipher = Cipher::bf_cbc();
    let decrypted = decrypt(cipher, &key.as_slice()[..cipher.key_len()], None, data.as_slice())?;
    let mut zlib = ZlibDecoder::new(decrypted.as_slice());
    let mut uncompressed = Vec::new();
    zlib.read_to_end(&mut uncompressed)?;

    match resolve_attachments(&path, &output_dir, uncompressed.as_slice(), key.as_slice()) {
        Ok((resolved, errors)) => {
            if !errors {
                // info!("Recovered: {}", output_path.to_str().unwrap());
                fs::File::create(output_path)?.write_all(&resolved)?;
            } else {
                let modified = modify_subject(resolved.as_slice(), "[uszkodzony] ");
                let file_stem = output_path.file_stem().unwrap().to_str().unwrap().to_string();
                output_path.pop();
                output_path.push(format!("bad-{}.eml", &file_stem));
                error!("Missing attachment(s): {}", output_path.to_str().unwrap());
                trace!("Source: {:?}", path);
                fs::File::create(&output_path)?.write_all(modified.as_slice());
            }
        },
        Err(msg) => {
            error!("Attachment processing failed: {}, skipping: {:?}", msg, path);
        }
    }

    Ok(1)
}

fn read_key(path: impl AsRef<str>) -> AppResult<Vec<u8>> {
    let p: PathBuf = PathBuf::from(path.as_ref());
    let mut f = fs::File::open(p)?;
    let mut vec = Vec::new();
    let size = f.metadata()?.len();
    if size > 4096 {
        return AppError::generic("Key file is too large");
    }

    f.read_to_end(&mut vec)?;

    Ok(vec)
}

fn read_file(path: impl AsRef<str>) -> AppResult<Vec<u8>> {
    let p: PathBuf = PathBuf::from(path.as_ref());
    let mut f = fs::File::open(p)?;
    let mut vec = Vec::new();
    f.read_to_end(&mut vec)?;
    Ok(vec)
}

fn delay_busy_thread(tpool: &ThreadPool) {
    while tpool.queued_count() > 10000 {
        sleep(Duration::from_millis(50));
    }
}

pub fn run(args: &DecryptArgs) -> AppResult<()> {
    let cores = args.num_cores.unwrap_or(num_cpus::get());
    let pool = ThreadPool::new(cores);

    let p: PathBuf = PathBuf::from(&args.piler_key_path);
    if !p.exists() {
        error!("Provided key file doesn't exist: {}", args.piler_key_path);
        return AppError::generic("Key file doesn't exist");
    }

    let mut count = 0;
    let max_count = args.limit.unwrap_or(0);
    let key = Arc::new(read_key(&args.piler_key_path)?);
    for entry in WalkDir::new(&args.store_path) {
        let entry = entry?;
        if !entry.file_type().is_file() { continue; }
        let name = entry.file_name().to_str().unwrap().to_string();
        if !name.ends_with(".m") { continue; }
        let path = entry.path().to_path_buf();
        let output_path = args.output_path.clone();
        let arc_key = key.clone();

        let mut test_output_path = PathBuf::new();
        test_output_path.push(&output_path);
        test_output_path.push(format!("{}.eml", path.file_stem().unwrap().to_str().unwrap()));
        if test_output_path.exists() {
            trace!("Skipping {:?}", test_output_path);
            continue;
        }

        if cores < 2 {
            decrypt_file(path, arc_key, output_path).expect("B");
        } else {
            pool.execute(|| {
                decrypt_file(path, arc_key, output_path).expect("B");
            });

            delay_busy_thread(&pool);
        }

        count += 1;
        if max_count > 0 && count >= max_count {
            info!("Triggered a command-line limit count, breaking");
            break;
        }
    }

    pool.join();
    Ok(())
}

fn modify_subject(data: &[u8], prefix: &str) -> Vec<u8> {
    let pattern = "\x0D\x0ASubject: ".as_bytes();
    let search = TwoWaySearcher::new(pattern);
    if let Some(pos) = search.search_in(data) {
        let part_a = &data[0..(pos + pattern.len())];
        let part_b = &data[(pos + pattern.len())..];

        let mut rendered = Vec::new();
        rendered.write_all(part_a);
        rendered.write_all(prefix.as_bytes());
        rendered.write_all(part_b);
        rendered
    } else {
        warn!("No Subject in message, is the format OK?");
        return data.to_vec();
    }
}
