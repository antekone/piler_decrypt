use std::fs::OpenOptions;
use std::path::Path;
use mmap_rs::{Mmap, MmapOptions};
use crate::AppResult;

pub fn file_contents(path: &Path) -> AppResult<Mmap> {
    let file = OpenOptions::new().read(true).write(false).open(&path)?;
    let data = unsafe {
        MmapOptions::new(file.metadata()?.len() as usize)
            .with_file(file, 0u64)
            .map()?
    };

    Ok(data)
}