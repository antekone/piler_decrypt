use std::fs;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::time::Instant;
use log::{error, info, trace, warn};
use mail_parser::Message;
use memmem::TwoWaySearcher;
use walkdir::WalkDir;
use serde::{Serialize, Deserialize};
use crate::{AppResult, Args, utils};

/// Create sender mailboxes from generated EML repository
#[derive(Args)]
pub struct SortArgs {
    /// Path to the directory with EML files (files need the `.eml` extension)
    #[arg(short, long, value_name = "DIR PATH")]
    repo_path: String,

    /// Path to the output sorted Maildir root directory
    #[arg(short, long, value_name = "DIR PATH")]
    maildir_root: String,

    #[arg(long, value_name = "FILE PATH")]
    snapshot_path: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
struct Snapshot {
    counter: usize
}

fn extract_headers(data: &[u8]) -> AppResult<Vec<String>> {
    let mut lines = Vec::<String>::new();
    let mut cur_string = String::new();
    let mut maybe_continuation = false;

    for b in data {
        if b == &b'\n' {
            if maybe_continuation == true {
                lines.push(cur_string.to_ascii_lowercase());
                return Ok(lines);
            } else {
                maybe_continuation = true;
                continue;
            }
        }
        if b == &b'\r' { continue; }

        if maybe_continuation {
            maybe_continuation = false;

            if b == &b' ' || b == &b'\t' {
                cur_string.push(' ');
                continue;
            } else {
                lines.push(cur_string.to_ascii_lowercase());
                cur_string = String::new();
            }
        }

        cur_string.push(*b as char);
    }

    lines.push(cur_string.to_ascii_lowercase());
    Ok(lines)
}

fn get_header(name: impl AsRef<str>, headers: &Vec<String>) -> Option<&str> {
    let name: &str = name.as_ref();
    for line in headers {
        if line.starts_with(name) {
            return Some(&line[name.len()..]);
        }
    }

    None
}

fn extract_email(line: &str) -> Option<&str> {
    let mut index_start: usize = 0;
    let mut index_end: usize = line.len();

    let line_bytes = line.as_bytes();
    for i in 0..line_bytes.len() {
        if line_bytes[i] == b'<' {
            index_start = i + 1;
        } else if line_bytes[i] == b'>' {
            index_end = i;
        }
    }

    Some(&line[index_start..index_end])
}

fn dedup(v: Vec<&str>) -> Vec<&str> {
    let mut v2 = Vec::<&str>::new();
    for item in v {
        if !v2.contains(&item) {
            v2.push(item);
        }
    }
    v2
}

fn extract_emails(line: &str, default_push: bool) -> Option<Vec<&str>> {
    let mut index_start: usize = 0;
    let mut index_end: usize = line.len();
    let mut emails = Vec::<&str>::new();

    let line_bytes = line.as_bytes();
    for i in 0..line_bytes.len() {
        if line_bytes[i] == b'<' {
            index_start = i + 1;
        } else if line_bytes[i] == b'>' {
            index_end = i;
            emails.push(&line[index_start..index_end].trim());
        }
    }

    if default_push && emails.is_empty() {
        emails.push(line.trim());
    }

    Some(emails)
}

fn dump_received(headers: &Vec<String>) {
    for h in headers {
        if h.starts_with("received") {
            info!("Received: {}", h);
        }
    }
}

fn store_to_maildir(mailbox: &str, maildir_root: &str, email: &str, eml_path: &Path) -> AppResult<()> {
    let eml_name = eml_path.file_name().unwrap().to_str().unwrap();
    let email2 = email.trim().replace("/", "");

    let mut eml_link_path = PathBuf::new();
    eml_link_path.push(maildir_root);
    eml_link_path.push(email2.trim());
    eml_link_path.push(mailbox);

    let mut eml_link_dir = eml_link_path.clone();
    eml_link_path.push(eml_name);

    if eml_link_path.exists() {
        return Ok(())
    }

    if !eml_link_dir.exists() {
        fs::create_dir_all(eml_link_dir)?;
    }

    let full_eml_path = fs::canonicalize(eml_path)?;
    trace!("symlink {:?} -> {:?}", full_eml_path, eml_link_path);
    let _ = std::os::unix::fs::symlink(full_eml_path, eml_link_path);
    Ok(())
}

fn process_eml(eml_path: &Path, args: &SortArgs) -> AppResult<()> {
    let mut all_emails_to = Vec::<&str>::new();
    let data = utils::file_contents(&eml_path)?;
    if data.starts_with("require [\"".as_bytes()) {
        warn!("This looks like a sieve script, not an EML file: {:?}", eml_path);
        return Ok(())
    }

    let headers = extract_headers(data.as_slice())?;

    // trace!("{:?}", headers);

    let hdr_from = {
        match get_header("from:", &headers) {
            Some(value) => value,
            None => {
                match get_header("x-envelope-from:", &headers) {
                    Some(value) => value,
                    None => {
                        match get_header("return-path:", &headers) {
                            Some(value) => value,
                            None => {
                                println!("{:?}", headers);
                                panic!("{:?} -- can't extract `from`!", eml_path);
                            }
                        }
                    }
                }
            }
        }
    };

    let email_from = extract_email(hdr_from);
    if email_from.is_none() {
        println!("{:?}", headers);
        panic!("{:?} -- can't extract e-mail from `from`!", eml_path);
    }

    if let Some(hdr_to) = get_header("to:", &headers) {
        let emails_to = extract_emails(hdr_to, true);
        for emails in emails_to.unwrap() {
            for x in emails.split(",") {
                all_emails_to.push(x.trim());
            }
        }
    }

    if let Some(hdr_cc) = get_header("cc:", &headers) {
        let emails_cc = extract_emails(hdr_cc, true);
        for emails in emails_cc.unwrap() {
            for x in emails.split(",") {
                all_emails_to.push(x.trim());
            }
        }
    }

    for header in &headers {
        if header.starts_with("received:") {
            let received_emails = extract_emails(&header[9..], false);
            for x in received_emails.unwrap() {
                all_emails_to.push(x.trim());
            }
        }
    }

    if all_emails_to.is_empty() {
        error!("Don't know where this e-mail was sent! {:?}", eml_path);
    }

    all_emails_to = dedup(all_emails_to);

    let email_from = email_from.unwrap().trim();

    trace!("    From: {}", email_from);
    trace!("    To: {:?}", all_emails_to);

    let maildir_root = args.maildir_root.as_str();

    store_to_maildir("sent/cur", maildir_root, email_from.trim(), eml_path)?;

    for email_to in all_emails_to {
        store_to_maildir("inbox/cur", maildir_root, email_to.trim(), eml_path)?;
    }

    Ok(())
}

fn dump_snapshot(path: impl AsRef<str>, mut counter: usize) -> AppResult<()> {
    if counter > 1 {
        counter -= 1;
    }

    let object = serde_json::to_string(&Snapshot { counter })?;
    let mut fw = OpenOptions::new().write(true).create(true).open(path.as_ref())?;
    fw.write(object.as_bytes())?;
    Ok(())
}

fn load_snapshot(path: impl AsRef<str>) -> AppResult<Snapshot> {
    Ok(serde_json::from_slice(utils::file_contents(
        PathBuf::from(path.as_ref()).as_path()
    )?.as_slice())?)
}

pub fn run(args: &SortArgs) -> AppResult<()> {
    let mut items = 0;
    let mut snapshot_counter = 0;
    let mut elapsed = Instant::now();
    let mut requested_snapshot_counter = 0;

    if let Some(path) = args.snapshot_path.as_ref() {
        let snapshot = load_snapshot(path).unwrap_or({
            Snapshot { counter: 0 }
        });

        requested_snapshot_counter = snapshot.counter;
        if requested_snapshot_counter > 0 {
            info!("Resuming from file {}...", requested_snapshot_counter);
        } else {
            info!("Starting from the beginning...");
        }
    }

    let mut displayed_resume_ok = false;
    for entry in WalkDir::new(&args.repo_path) {
        snapshot_counter += 1;
        if requested_snapshot_counter >= snapshot_counter {
            continue;
        } else {
            if !displayed_resume_ok {
                info!("Got resume point, starting parse");
                displayed_resume_ok = true;
            }
        }

        let entry = entry?;
        if !entry.file_type().is_file() { continue; }
        let name = entry.file_name().to_str().unwrap().to_string();
        if !name.ends_with(".eml") { continue; }
        let path = entry.path().to_path_buf();

        trace!("Parsing EML file: {:?}", path);

        match process_eml(path.as_path(), args) {
            Ok(_) => (),
            Err(msg) => {
                error!("{:?}", msg);
                error!("During processing of this EML file: {:?}", path);
                return Err(msg);
            }
        }

        items += 1;

        let millis = Instant::now().duration_since(elapsed).as_millis();
        if millis > 10_000 {
            info!("Processing speed of last 10s: {} EML files, last file number: {}", items, snapshot_counter);
            items = 0;
            elapsed = Instant::now();

            if let Some(path) = args.snapshot_path.as_ref() {
                dump_snapshot(path, snapshot_counter);
            }
        }
    }

    if let Some(path) = args.snapshot_path.as_ref() {
        dump_snapshot(path, snapshot_counter);
    }
    
    info!("Done!");
    Ok(())
}